"""Script to connect adb shell over IP which you find using a mac address"""

from os import popen, path, listdir
from sys import stderr, exit as ext
from xml.etree import ElementTree
from json import loads as json_loads, dumps as json_dumps
from json.decoder import JSONDecodeError
import re
import argparse
import netifaces as nif
from colorama import Fore


def cprint(text, color=Fore.GREEN):
    """Print colored text to STDERR"""
    stderr.write(f"{color}{text}{Fore.RESET}")


def connect(args, result):

    cmd = f"adb connect {result['ipv4']}:5555"
    report = popen(cmd).read()
    cprint(
        report,
        Fore.RED if "failed" in report else Fore.GREEN,
    )
    if args.print_mac:
        cprint("MAC: " + result["mac"].upper() + "\n")

    if ip_parser.match(report):
        if ":" in report:
            report = report[: report.index(":")]

        ip_address = ip_parser.sub(r"\1", report)
        if ip_address and args.alias_as_adb:
            if args.verbose:
                cprint(f"alias adb='adb -s {ip_address}'\n", Fore.CYAN)
            print(f"alias adb='adb -s {ip_address}'")
        if ip_address and args.name and args.alias:
            if args.verbose:
                cprint(
                    f"alias {args.name}='adb -s {ip_address}'\n",
                    Fore.CYAN,
                )
            print(f"alias {args.name}='adb -s {ip_address}'")


def open_cached(rw):
    file_name = "cached_ip_addresses"
    try:
        popen("ls ~/.cache/maccon").read().split("\n").index(file_name)
    except ValueError:
        cprint(f"creating ~/.cache/maccon/{file_name}..")
        popen("mkdir -p ~/.cache/maccon")
        popen(f"touch ~/.cache/maccon/{file_name}")
    return open(f'{popen("echo -n ~").read()}/.cache/maccon/{file_name}', rw)


def main():
    """Main function"""
    cached = {}
    with open_cached("r") as cache_file:
        try:
            cached = json_loads(cache_file.read())
        except JSONDecodeError:
            cached = {}
    if args.name in cached:
        args.mac = cached[args.name]["scan_macs"]

    if args.name and not args.mac:
        cprint("You have provided no MAC addresses and none are cached!", Fore.RED)
        ext(1)

    # exit if nmap program cannot be found
    if popen("which nmap > /dev/null && echo 1 || echo 0").read() == "0":
        if popen("sudo which nmap > /dev/null && echo 1 || echo 0").read() == "0":
            cprint(
                ">>> nmap is not installed! please install and try again!\n", Fore.RED
            )
            ext(1)

    # get the gateway IP address
    if args.gateway:
        if not ip_parser.match(args.gateway):
            cprint("The gateway MAC address is of an incorrect format!\n", Fore.RED)
            ext(1)
        IP_ADDRESS = args.gateway
    else:
        IP_ADDRESS = nif.gateways()["default"][nif.AF_INET][0]

    if "/" not in IP_ADDRESS:
        try:
            IP_ADDRESS += (
                f"/{(['10', '172', '192'].index(IP_ADDRESS.split('.')[0])+1)*8}"
            )
        except ValueError:
            IP_ADDRESS += "/24"

    err_count = 0
    while err_count < 7:
        # read all mac addresses and put the in tmp file
        error = popen(
            f"sudo nmap -sn {IP_ADDRESS} -oX /tmp/nmap.xml 2> /dev/null > /dev/null && echo 1 || echo 0"
        ).read()
        if error == "0":
            cprint(">>> An nmap error occured! Try again\n", Fore.RED)
            ext(1)

        try:
            with open("/tmp/nmap.xml", "r", encoding="utf-8") as xmlfile:
                nmap_scan = ElementTree.fromstring(xmlfile.read())
                # find every tag with hosts
                hosts = [e for e in nmap_scan if e.tag == "host"]

                # get all  adddress attributes from the host
                attributes = [
                    [x.attrib for x in list(e) if "addr" in x.attrib] for e in hosts
                ]

                # then get all addresses that have both the mac address and the ipv4 address
                # and finally get sirted into a list of dicts ith 'ipv4' and 'mac' as keys
                addresses = [c for c in attributes if len(c) > 1]
                addresses = [
                    {el["addrtype"]: el["addr"] for el in a} for a in addresses
                ]

            for MAC in args.mac.split(","):
                results = [
                    addr for addr in addresses if MAC.lower() in addr["mac"].lower()
                ]
                if results:

                    connect(args, results[0])

                    if args.name:
                        with open_cached("w") as cache_file:
                            cached[args.name] = results[0]
                            cached[args.name]["scan_macs"] = args.mac
                            cache_file.write(json_dumps(cached, indent=4))

        except ElementTree.ParseError:
            err_count += 1
            cprint(
                f"[ATTEMPT: {err_count}/6] >>> An nmap XML parse error occured! Trying again...\n",
                Fore.CYAN,
            )
            if err_count < 6:
                continue

            cprint("\n>>> An nmap XML parse error could NOT be resolved!\n", Fore.RED)
            ext(1)

        break


ip_parser = re.compile(r"[^0-9]*([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}).*")

parser = argparse.ArgumentParser(
    """helps with conneting to boxes using nmap nad the STB's full or partial MAC address.
Verbose text is output to the STDERR, and aliases to STDOUT
 so that the script's outp can be run under 'eval'

"""
)
parser.add_argument(
    "-m", "--mac", type=str, help="Add one or multiple mac addresses separated by ','"
)
parser.add_argument("-g", "--gateway", type=str, help="local network gateway")
parser.add_argument(
    "-A", "--alias-as-adb", action="store_true", help="alias adb='adb -s [IP]'"
)
parser.add_argument(
    "-a", "--alias", action="store_true", help="alias the MAC addres under its name"
)
parser.add_argument(
    "-v",
    "--verbose",
    action="store_true",
    help="Print out all aliases made for this board",
)
parser.add_argument(
    "-M", "--print-mac", action="store_true", help="print out the mac address"
)
parser.add_argument("name", metavar="N", type=str, nargs="*")

# parse arguments
args = parser.parse_args()
args.name = " ".join(args.name)


if __name__ == "__main__":
    main()
