"""Script to connect adb shell over IP which you find using a mac address"""
import sys
from .maccon import *

__author__ = "Borno Stojak (borno.stojak@uniqcast.com)"
__copyright__ = "Borno Stojak"
__license__ = "Apache"

if sys.version_info[:2] >= (3, 8):
    from importlib.metadata import PackageNotFoundError, version  # pragma: no cover
else:
    from importlib_metadata import PackageNotFoundError, version  # pragma: no cover

try:
    # Change here if project is renamed and does not equal the package name
    dist_name = __name__
    __version__ = version(dist_name)
except PackageNotFoundError:  # pragma: no cover
    __version__ = "unknown"
finally:
    del version, PackageNotFoundError
