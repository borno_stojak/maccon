=========
Changelog
=========

Version 0.3.1
===========

- Works with normal user
- Requires root privilages
- Was NOT tested on user reqiering a password every time he runs sudo
  

Version 0.3.2
===========

- Exit the application on nmap crash


Version 0.3.3
===========

- Documentationi revision
- Error handling in XML parsing of nmap scan


Version 0.3.4
===========

- Added "-s/--spit" argument that "spits" out the full MAC addres of the box
