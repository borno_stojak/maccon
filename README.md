# INSTALL

To install this run `pip install git+https://bitbucket.org/borno_stojak/maccon`
You will need `git` and `pip` installed to install it this way.

If you want to run it using the `maccon` command add the followin to your .bashrc or whatever the rc file for your shell is:

__Note that this will work with bash but is not guaranteed to work for other shells__

```
function maccon() {
    eval $(python -m maccon $@)
}
```
